import Vue from 'vue'
import AppComponent from 'components/app-component'
import './styles.scss'

new Vue({
  el: '#app',
  components: {
    'app-component': AppComponent
  }
})
