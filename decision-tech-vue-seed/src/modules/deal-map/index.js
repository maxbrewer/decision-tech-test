import _ from 'lodash'

function map (item) {
  return {
    title: item.title,
    mobile: mapMobile(item.mobile)
  }
}

function mapMobile (mobile) {
  if (!mobile) return null
  return {
    data: _.get(mobile, 'data.label', null),
    minutes: _.get(mobile, 'minutes.label', null),
    texts: _.get(mobile, 'texts.label', null),
    connectionType: _.get(mobile, 'connectionType.label', null)
  }
}


export default map
