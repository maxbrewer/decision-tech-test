import dealMap from './index.js'

describe('Deal map', () => {
  it('Maps the title', () => {
    const title = 'The title'
    expect(dealMap({ title: title }).title).toBe(title)
  })

  describe('Mapping the mobile offer', () => {
    it('Maps null', () => {
      const mapped = dealMap({ mobile: null })
      expect(mapped.mobile).toBe(null)
    })

    it('Maps the data', () => {
      const dataLabel = 'Data label'
      const mapped = dealMap({ mobile: { data: { label: dataLabel }}})
      expect(mapped.mobile.data).toBe(dataLabel)
    })

    it('Maps the minutes', () => {
      const minutes = 'Minutes'
      const mapped = dealMap({ mobile: { minutes: { label: minutes }}})
      expect(mapped.mobile.minutes).toBe(minutes)
    })

    it('Maps the texts', () => {
      const texts = 'Texts'
      const mapped = dealMap({ mobile: { texts: { label: texts }}})
      expect(mapped.mobile.texts).toBe(texts)
    })

    it('Maps the connection type', () => {
      const connectionType = 'Connection type'
      const mapped = dealMap({ mobile: { connectionType: { label: connectionType }}})
      expect(mapped.mobile.connectionType).toBe(connectionType)
    })
  })
})
