import _ from 'lodash'

function sameElements (first, second) {
  const equalLengths = first.length === second.length
  return equalLengths &&
    _.intersection(first, second).length === first.length
}

function filter (by, items) {
  console.log(by)
  console.log(items)

  const hasItems = items.length > 0
  const unfiltered = by.length === 0
  return hasItems && (
    unfiltered || sameElements(by, items)
  )
}

export default filter
