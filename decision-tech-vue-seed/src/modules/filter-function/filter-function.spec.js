import filter from './index.js'

describe('Filter', () => {
  describe('No filter', () => {
    it('returns false when given nothing', () => {
      var noFilter = function (item) { return filter([], item) }
      expect(noFilter([])).toBe(false)
    })

    it('returns true when given broadband', () => {
      var noFilter = function (item) { return filter([], item) }
      expect(noFilter(['Broadband'])).toBe(true)
    })

    it('returns true when given mobile', () => {
      var noFilter = function (item) { return filter([], item) }
      expect(noFilter(['Mobile'])).toBe(true)
    })
  })

  describe('Filter for broadband', () => {
    it('returns false when given nothing', () => {
      var broadbandFilter = function (item) { return filter(['Broadband'], item) }
      expect(broadbandFilter([])).toBe(false)
    })

    it('returns true when given broadband', () => {
      var broadbandFilter = function (item) { return filter(['Broadband'], item) }
      expect(broadbandFilter(['Broadband'])).toBe(true)
    })

    it('returns false when given mobile', () => {
      var broadbandFilter = function (item) { return filter(['Broadband'], item) }
      expect(broadbandFilter(['Mobile'])).toBe(false)
    })

    it('returns false when given broadband and mobile', () => {
      var broadbandFilter = function (item) { return filter(['Broadband'], item) }
      expect(broadbandFilter(['Broadband', 'Mobile'])).toBe(false)
    })
  })

  describe('filter by broadband and mobile', () => {
    it('returns false when given nothing', () => {
      var broadbandAndMobileFilter = function (item) { return filter(['Broadband', 'Mobile'], item) }
      expect(broadbandAndMobileFilter([])).toBe(false)
    })

    it('returns false when given broadband', () => {
      var broadbandAndMobileFilter = function (item) { return filter(['Broadband', 'Mobile'], item) }
      expect(broadbandAndMobileFilter(['Broadband'])).toBe(false)
    })

    it('returns false when given mobile', () => {
      var broadbandAndMobileFilter = function (item) { return filter(['Broadband', 'Mobile'], item) }
      expect(broadbandAndMobileFilter(['Mobile'])).toBe(false)
    })

    it('returns true when given broadband and mobile', () => {
      var broadbandAndMobileFilter = function (item) { return filter(['Broadband', 'Mobile'], item) }
      expect(broadbandAndMobileFilter(['Broadband', 'Mobile'])).toBe(true)
    })

    it('returns true when given mobile and broadband', () => {
      var broadbandAndMobileFilter = function (item) { return filter(['Broadband', 'Mobile'], item) }
      expect(broadbandAndMobileFilter(['Mobile', 'Broadband'])).toBe(true)
    })
  })
})
