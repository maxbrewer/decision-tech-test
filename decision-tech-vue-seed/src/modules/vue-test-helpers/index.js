import Vue from 'vue'

function renderComponent (Component, properties) {
  const Ctor = Vue.extend(Component)
  return new Ctor({ propsData: properties }).$mount()
}

function getElementsByTagName (viewModel, tag) {
  const result = []
  const elements = viewModel.$el.getElementsByTagName(tag)
  for (var i = 0; i < elements.length; i++) {
    result.push(elements.item(i))
  }
  return result
}

export default {
  renderComponent: renderComponent,
  getElementsByTagName: getElementsByTagName
}

export { renderComponent, getElementsByTagName }
