import GridRow from './index.vue'
import { renderComponent } from 'vue-test-helpers'

describe('Grid row component', () => {
  it('Has 7 children', () => {
    const vm = renderComponent(GridRow, { })
    expect(vm.$el.childElementCount).toBe(7)
  })

  it('Has deals in child one', () => {
    const title = 'The expected title'
    const vm = renderComponent(GridRow, { deal: { title: title }})
    expect(vm.$el.firstChild.textContent).toBe(title)
  })

  it('Has contract length in child 2', () => {
    const contractLength = 'The expected contract length'
    const vm = renderComponent(GridRow, { deal: { contractLength: contractLength }})
    expect(vm.$el.children[1].textContent).toBe(`${contractLength} Months`)
  })

  it('Has cost in child 7', () => {
    const cost = 'The expected cost'
    const vm = renderComponent(GridRow, { deal: { prices: [{ totalContractCost: cost }] }})
    expect(vm.$el.children[6].textContent).toBe(cost)
  })

  it('Has "N/A" in cell 3 when no extra data is provided', () => {
    const vm = renderComponent(GridRow, { })
    expect(vm.$el.children[3 - 1].textContent).toBe('N/A')
  })

  it('Has "N/A" in cell 5 when no extra data is provided', () => {
    const vm = renderComponent(GridRow, { })
    expect(vm.$el.children[5 - 1].textContent).toBe('N/A')
  })

  it('Has "N/A" in cell 6 when no extra data is provided', () => {
    const vm = renderComponent(GridRow, { })
    expect(vm.$el.children[6 - 1].textContent).toBe('N/A')
  })
})
