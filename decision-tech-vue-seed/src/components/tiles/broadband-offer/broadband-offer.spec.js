import BroadbandOffer from './index.vue'
import { renderComponent } from 'vue-test-helpers'

describe('Broadband offer component', () => {
  it('Shows "N/A" when no speed or usage are provided', () => {
    const vm = renderComponent(BroadbandOffer, {})
    expect(vm.$el.textContent).toBe('N/A')
  })

  it('Shows speed', () => {
    const speed = 333
    const vm = renderComponent(BroadbandOffer, { speed: speed })
    expect(vm.$el.textContent).toContain(speed + ' MB')
  })

  it('Shows usage', () => {
    const usage = 'usage'
    const vm = renderComponent(BroadbandOffer, { usage: usage })
    expect(vm.$el.textContent).toContain(usage)
  })
})
