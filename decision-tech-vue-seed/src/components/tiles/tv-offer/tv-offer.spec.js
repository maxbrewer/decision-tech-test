import TvOffer from './index.vue'
import { renderComponent, getElementsByTagName } from 'vue-test-helpers'

describe('Tv offer component', () => {
  it('displays "N/A" if there are no channels available.', () => {
    const vm = renderComponent(TvOffer, {})
    expect(vm.$el.textContent).toBe('N/A')
  })

  it('Shows a logo if there is a popular channel', () => {
    const channels = ['some-url.com']

    const vm = renderComponent(TvOffer, { logos: channels })

    const imageSources = getElementsByTagName(vm, 'img')
      .map(image => image.getAttribute('src'))
    channels.forEach(url => {
      expect(imageSources).toContain(url)
    })
  })

  it('Shows multiple popular channel logos', () => {
    const channels = ['first-url.com', 'second-url.com']

    const vm = renderComponent(TvOffer, { logos: channels })

    const imageSources = getElementsByTagName(vm, 'img')
      .map(image => image.getAttribute('src'))
    channels.forEach(url => {
      expect(imageSources).toContain(url)
    })
  })
})
