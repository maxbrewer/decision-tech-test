import MobileOffer from './index.vue'
import { renderComponent } from 'vue-test-helpers'

describe('Mobile offer component', () => {
  it('Shows "N/A" when no data, minutes, texts or connectionType are provided', () => {
    const vm = renderComponent(MobileOffer)
    expect(vm.$el.textContent).toBe('N/A')
  })

  it('Shows Data', () => {
    const data = 'data label'
    const vm = renderComponent(MobileOffer, { deal: { data: data }})
    expect(vm.$el.textContent).toContain(data)
  })

  it('Shows Minutes', () => {
    const minutes = 'minutes label'
    const vm = renderComponent(MobileOffer, { deal: { minutes: minutes }})
    expect(vm.$el.textContent).toContain(minutes)
  })

  it('Shows Texts', () => {
    const texts = 'texts label'
    const vm = renderComponent(MobileOffer, { deal: { texts: texts }})
    expect(vm.$el.textContent).toContain(texts)
  })

  it('Shows Connection Type', () => {
    const connectionType = 'connection type label'
    const vm = renderComponent(MobileOffer, { deal: { connectionType: connectionType }})
    expect(vm.$el.textContent).toContain(connectionType)
  })
})
