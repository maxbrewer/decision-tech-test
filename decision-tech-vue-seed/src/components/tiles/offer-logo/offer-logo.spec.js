import OfferLogo from './index.vue'
import { renderComponent, getElementsByTagName } from 'vue-test-helpers'

describe('Offer logo component', () => {
  it('Shows logo provided', () => {
    const logoUrl = 'some-url.com'
    const vm = renderComponent(OfferLogo, { logoUrl: logoUrl })

    const imageSources = getElementsByTagName(vm, 'img')
      .map(image => image.getAttribute('src'))

    expect(imageSources.length).toBe(1)
    expect(imageSources[0]).toBe(logoUrl)
  })
})
