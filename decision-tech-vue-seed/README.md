# DecisionTech Vue Seed

It's a bit late to carry on, so I'll call this a night.
There are a few things that I would do if I had more time.

* More testing.
    I would have liked to have set up a test harness with cucumber or somesuch and a web browser.
* More unit testing.
    this seems to have decreased as the night went on.
* Mobile layout.
    didn't get to this.
* Actually getting the filter working.
    A little annoying, but I'm far too tired to see what the problem its.
* Rearrange the grid component a little.
    I was planning on making this into a standalone module with a package json containing all the tiles.